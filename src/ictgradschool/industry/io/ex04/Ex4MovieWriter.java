package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieWriter;

import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        File newFile = new File(fileName);
        try (PrintWriter outFile = new PrintWriter(new FileWriter(newFile))){
            for(int i = 0; i < films.length; i++){
                outFile.write(films[i].getName()+" , "+ films[i].getYear()+" , "+ films[i].getLengthInMinutes()+" , "+films[i].getDirector()+ '\r');
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Movies saved successfully to " + fileName + "!");

        // TODO Implement this with a PrintWriter

    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
