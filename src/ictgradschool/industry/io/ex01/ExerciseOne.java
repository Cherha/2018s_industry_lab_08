package ictgradschool.industry.io.ex01;

import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        //fileReaderEx01();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        try {

            FileReader fileReader = new FileReader("input2.txt");
            int i = -1;
             while ((i = fileReader.read())!= -1){
                 total++;
                 if((char)i == 'e' || (char)i == 'E'){
                     numE++;
                 }
             }
             fileReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void fileReaderEx01() {
        int num = 0;
        FileReader fR = null;
        try {
            fR = new FileReader("input1.txt");
            num = fR.read();
            System.out.println(num);
            System.out.println(fR.read());
            System.out.println(fR.read());
            System.out.println(fR.read());
            System.out.println(fR.read());
            fR.close();
        } catch(IOException e) {
            System.out.println("IO problem");
        }
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        try (BufferedReader bR = new BufferedReader(new FileReader("input2.txt"))) {
            String str;

            while ((str = bR.readLine()) != null) {
                for (int i = 0; i < str.length(); i++) {
                    total++;
                    if (str.charAt(i) == 'e' || str.charAt(i) == 'E') {
                        numE++;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
