package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.*;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        File userFile = new File(fileName);
        try (BufferedReader reader = new BufferedReader(new FileReader(userFile))) {

            String num = null;
            while ((num = reader.readLine())!= null){
                System.out.println(num);

            }



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Close the reader
        }
}


    public static void main(String[] args) {
        new MyReader().start();
    }
}
