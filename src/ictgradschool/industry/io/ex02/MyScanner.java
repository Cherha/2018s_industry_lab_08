package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        File userFile = new File(fileName);
        try (Scanner scan = new Scanner(userFile)) {


            while (scan.hasNextLine()){
                System.out.println(scan.nextLine());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Close the reader
        }
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
